# Changelog

## 0.1.4 (2025-01-16)

#### Fixes

* not running on test_x.py files

## 0.1.3 (2024-06-10)

#### Fixes

* remove ini and cfg file rules

Full set of changes: [`0.1.1...0.1.3`](https://bitbucket.org/thoughtfulautomation/ta-commit-secure/compare/0.1.1...0.1.3)

## 0.1.1 (2021-12-30)

#### Fixes

* run only on staged files

Full set of changes: [`0.1.0...0.1.1`](https://bitbucket.org/thoughtfulautomation/ta-commit-secure/compare/0.1.0...0.1.1)

## 0.1.0 (2021-12-20)

