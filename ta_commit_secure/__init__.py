from typing import Iterator


def secrets(arguments: str) -> Iterator:
    """
    Boilerplate for simplifying usage and integration.

    import ta_commit_secure
    for secret in ta_commit_secure.secrets("-r apikey tests/fixtures"):
        print(secret)
    """
    import shlex

    from ta_commit_secure.core.args import parse_args
    from ta_commit_secure.main import run

    argv = shlex.split(arguments)
    return run(parse_args(argv))
