import sys
from argparse import Namespace
from itertools import chain
from os import environ
from typing import Iterator

from ta_commit_secure.core.args import parse_args
from ta_commit_secure.core.config import load_config
from ta_commit_secure.core.log import configure_log
from ta_commit_secure.core.pairs import make_pairs
from ta_commit_secure.core.printer import printer
from ta_commit_secure.core.rules import load_rules
from ta_commit_secure.core.scope import load_scope
from ta_commit_secure.core.secrets import detect_secrets
from ta_commit_secure.models.pair import KeyValuePair

environ["PYTHONIOENCODING"] = "UTF-8"


def cli():  # pragma: no cover
    """Main method when executing from CLI given argv"""
    args = parse_args()
    secrets = run(args)
    found_secrets = list(map(lambda secret: printer(args, secret), secrets))
    sys.exit(len(found_secrets))


def run(args: Namespace) -> Iterator[KeyValuePair]:
    """Main method for getting secrets given args"""
    configure_log(args)

    config = load_config(args)
    rules = load_rules(args, config)
    scope = load_scope(args, config)
    parsed = map(lambda file: make_pairs(config, file), scope)
    detected = map(lambda pairs: detect_secrets(rules, pairs), parsed)
    secrets = chain.from_iterable(detected)

    return secrets


if __name__ == "__main__":
    cli()
