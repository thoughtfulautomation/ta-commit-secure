from os import remove, urandom
from pathlib import Path
from tempfile import gettempdir

import pytest

from tests.unit.conftest import fixture_path
from ta_commit_secure.core.args import parse_args
from ta_commit_secure.core.log import configure_log, global_exception_handler


@pytest.mark.parametrize(("create", "expected"), [(False, None), (True, Path(gettempdir()).joinpath("ta_commit_secure.log"))])
def test_configure_log(create, expected):
    args = []
    if create:
        args = ["--log"]

    args.append(fixture_path())
    result = configure_log(parse_args(args))
    if result:
        assert result.exists()
        try:
            remove(result)

        except PermissionError:
            """Patch for Github Actions windows-latest permissions"""

    assert result == expected


def test_global_exception_handler():
    args = parse_args(["-l", fixture_path()])
    logfile = configure_log(args)
    message = urandom(30).hex()
    try:
        1 / 0

    except Exception:
        global_exception_handler(logfile.as_posix(), message)

    logtext = logfile.read_text()

    assert "ZeroDivisionError: division by zero" in logtext
    assert message in logtext
