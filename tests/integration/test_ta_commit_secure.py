import pytest

import ta_commit_secure
from tests.unit.conftest import config_path, fixture_path


@pytest.mark.parametrize(
    ("args", "expected"),
    [
        (f"-c {config_path('integration.yml')} {fixture_path()}", 3),
        (f"-r apikey-known {fixture_path('apikeys-known.yml')}", 54),
        (f"--rules file-known {fixture_path('files')}", 3),
        (f"-s BLOCKER {fixture_path('aws.yml')}", 3),
    ],
)
def test_ta_commit_secure(args, expected):
    result = list(ta_commit_secure.secrets(args))
    assert len(result) == expected
